const { resizeSharp, resizeJimp } = require('./resize');
const express = require('express')
const multer = require('multer');
const path = require('path');
const fs = require('fs');

const app = express();

var server = app.listen(8000, () => {
    console.log("Server started");
    var host = server.address().address === '::' ? 'localhost' : server.address().address;
    var port = server.address().port;
    console.log("Server is listening at http://%s:%s", host, port);
})

let fileName;

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads');
    },
    filename: function (req, file, cb) {
        fileName = file.originalname;
        cb(null, fileName);
    }
})

var upload = multer({
    storage: storage,
    fileFilter: function(req, file, callback) {
        var ext = path.extname(file.originalname);
        if (ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext != '.bmp') {
            return callback( /*res.end('Only images are allowed')*/ null, false)
        }

        callback(null, true);
    }
})

app.post('/resize', upload.single('file'), (req, res, next) => {
    const file = req.file
    if (!file) {
        const error = new Error('Please upload a file');
        error.httpStatusCode = 400;
        return next(error);
    }
    const widthString = req.query.width;
    const heightString = req.query.height;
    if (!widthString || !heightString) {
        const error = new Error('Please specify the width and height');
        error.httpStatusCode = 400;
        return next(error);
    }

    const dimension = {
        width: +widthString,
        height: +heightString
    }

    var imgPath = "./uploads/" + fileName;
    var resizePath = "./resized/" + fileName;

    var mime = {
        jpg: 'image/jpeg',
        jpeg: 'image/jpeg',
        png: 'image/png',
        bmp: 'image/bmp'
    };

    const type = mime[path.extname(resizePath).slice(1)] || 'text/plain';
    var resizeMethod;
    if (type === 'image/png' || type === 'image/bmp') {
        resizeMethod = resizeJimp;
        resizePath = resizePath.slice(0, -4) + '.jpg'; // Convert to JPEG for lighter image size
    } else {
        resizeMethod = resizeSharp;
    }

    resizeMethod(imgPath, resizePath, dimension, (err, result)=>{
        const s = fs.createReadStream(resizePath);
        if(err) {
            res.set('Content-Type', 'text/plain');
            res.status(404).end('Not found');
        } else {
            s.on('open', function () {
                res.set('Content-Type', type);
                s.pipe(res);
            });
            s.on('error', function () {
                res.set('Content-Type', 'text/plain');
                res.status(404).end('Not found');
            });
        }
        clearImages('./resized');
        clearImages('./uploads');
    });
})


function clearImages(directory) {
    fs.readdir(directory, (err, files) => {
        if (err) throw err;
        for (const file of files) {
            const ext = path.extname(file);
            if (ext === '.jpg' || ext === '.png') {
                fs.unlink(path.join(directory, file), err => {
                    if (err) throw err;
                });
            }
        }
    });
}